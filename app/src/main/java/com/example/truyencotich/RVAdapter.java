package com.example.truyencotich;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.truyencotich.model.StoryCard;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.StoryViewHolder> {

    List<StoryCard> stories;

    public RVAdapter(List<StoryCard> stories) {
        this.stories = stories;
    }

    @NonNull
    @Override
    public StoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_item, viewGroup, false);
        StoryViewHolder storyViewHolder = new StoryViewHolder(view);
        return storyViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull StoryViewHolder storyViewHolder, int i) {
        storyViewHolder.title.setText(stories.get(i).getTitle());
        Picasso.get().load(stories.get(i).getImageUrl()).placeholder(R.drawable.ic_wifi_blue_24dp).error(R.drawable.ic_warning_red_24dp).into(storyViewHolder.image);
        storyViewHolder.cv.setTag(i);
    }

    @Override
    public int getItemCount() {
        return stories.size();
    }


    public static class StoryViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView title;
        ImageView image;


        public StoryViewHolder(@NonNull View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            title = itemView.findViewById(R.id.tv_story_title);
            image = itemView.findViewById(R.id.iv_story_photo);
        }
    }

}
