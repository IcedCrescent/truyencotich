package com.example.truyencotich;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.truyencotich.dal.DatabaseManager;
import com.example.truyencotich.model.StoryCard;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvStories;
    private List<StoryCard> stories;
    private CardView cvStory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvStories = findViewById(R.id.rv_stories);
        cvStory = findViewById(R.id.cv);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvStories.setLayoutManager(layoutManager);
        rvStories.setHasFixedSize(true);

//        cvStory.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, StoryActivity.class);
//                intent.putExtra("story", stories.get((int) v.getTag()));
//                startActivity(intent);
//            }
//        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        initData();
        initAdapter();
    }

    public void initData() {
        stories = DatabaseManager.getInstance(this).getAllStory();
    }

    private void initAdapter() {
        RVAdapter adapter = new RVAdapter(stories);
        rvStories.setAdapter(adapter);
    }

    public void positionAction(View view) {
        Intent intent = new Intent(this, StoryActivity.class);
        intent.putExtra("story", stories.get((int) view.getTag()));
        startActivity(intent);
    }
}
