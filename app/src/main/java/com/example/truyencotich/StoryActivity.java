package com.example.truyencotich;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.truyencotich.model.StoryCard;
import com.squareup.picasso.Picasso;

public class StoryActivity extends AppCompatActivity {

    StoryCard story;
    ImageView image;
    TextView content;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story);

        story = (StoryCard) getIntent().getSerializableExtra("story");
        setupUI();
    }

    private void setupUI() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(story.getTitle());
        }

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(story.getTitle());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        image = findViewById(R.id.image);
        content = findViewById(R.id.content);

        Picasso.get().load(story.getImageUrl())
                .placeholder(R.drawable.ic_wifi_blue_24dp)
                .error(R.drawable.ic_warning_red_24dp)
                .into(image);
        content.setText(story.getContent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.voice) {
            // Handle voice over here
            Toast.makeText(this, "Playing", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
